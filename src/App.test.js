/**
 * @jest-environment jsdom
 */
import React from 'react';
import { mount } from 'enzyme'
import App from './App';

test("renders app correctly", () => {
  const tree = mount(<App />);
    expect(tree).toMatchSnapshot();
});