import * as React from "react";
import Authors from "./author/Authors";
import Books from "./book/Books";
import ResponsiveAppBar from "./AppBar";
const Home = () => {
  const [anchorElNav, setAnchorElNav] = React.useState("Books");

  function renderSwitch() {
    switch (anchorElNav) {
      case "Books":
        return <Books />;
      case "Authors":
        return <Authors />;
    }
  }
  return (
    <>
      <ResponsiveAppBar
        anchorElNav={anchorElNav}
        setAnchorElNav={setAnchorElNav}
      />
      {renderSwitch()}
    </>
  );
};

export default Home;
