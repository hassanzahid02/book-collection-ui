import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Home from "./Home";

const Index = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
    </Switch>
  );
};
export default Index;
