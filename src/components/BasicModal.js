import * as React from "react";
import Modal from "@mui/material/Modal";
import EditView from "./book/EditView";
import DeleteView from "./book/DeleteView";
import AuthorsView from "./book/AuthorsView";
import CreateView from "./book/CreateView";
import AuthorEditView from "./author/AuthorEditView";
import AuthorDeleteView from "./author/AuthorDeleteView";
import AuthorCreateView from "./author/AuthorCreateView";

export default function BasicModal({
  setOpen,
  reload,
  setReload,
  data,
  props,
}) {
  const handleClose = () => {
    setOpen({ ...data, status: false });
    setReload(!reload);
  };

  function renderSwitch() {
    switch (data.target) {
      case "edit-book":
        return <EditView row={data.data} />;
      case "delete-book":
        return <DeleteView row={data.data} />;
      case "authors":
        return <AuthorsView row={data.data} />;
      case "add-book":
        return <CreateView />;
      case "add-author":
        return <AuthorCreateView />;
      case "edit-author":
        return <AuthorEditView row={data.data} />;
      case "delete-author":
        return <AuthorDeleteView row={data.data} />;
    }
  }

  return (
    <div>
      <Modal
        open={data.status}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        {renderSwitch()}
      </Modal>
    </div>
  );
}
