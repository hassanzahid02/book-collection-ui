import * as React from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { CircularProgress } from "@mui/material";
import { deleteAuthor } from "../../api/graphql/mutation/Author";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function AuthorDeleteView({ row, ...props }) {
  const [isLoading, setIsLoading] = React.useState({ status: true, msg: "" });

  React.useEffect(() => {
    deleteAuthor({ setIsLoading: setIsLoading, id: row.id });
  }, []);

  return (
    <Box sx={style}>
      {isLoading.status ? (
        <CircularProgress />
      ) : (
        <Typography id="modal-modal-description">{isLoading.msg}</Typography>
      )}
    </Box>
  );
}
