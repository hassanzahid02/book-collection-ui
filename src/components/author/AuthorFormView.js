import * as React from "react";
import Box from "@mui/material/Box";
import { TextField } from "@mui/material";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import AdapterDateFns from "@mui/lab/AdapterDateFns";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function AuthorFormView({ data, setData, onClick }) {
  function validateInputs() {
    return data.firstName == "" || data.surName == "" || data.dob == "";
  }
  return (
    <>
      <div>
        {data.id && <TextField id="Id" label="Id" value={data.id} disabled />}
        <TextField
          id="First Name"
          label="First Name"
          value={data.firstName}
          required
          onChange={(e) => setData({ ...data, firstName: e.target.value })}
        />
        <TextField
          id="Last Name"
          label="Last Name"
          value={data.surName}
          required
          onChange={(e) => setData({ ...data, surName: e.target.value })}
        />
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DatePicker
            label="Date of Birth"
            value={data.dob}
            onChange={(newValue) => {
              setData({ ...data, dob: newValue });
            }}
            required
            renderInput={(params) => <TextField {...params} />}
          />
        </LocalizationProvider>
      </div>
      <div
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          margin: "auto",
          display: "flex",
        }}
      >
        <Box
          component="button"
          sx={{
            color: "white",
            backgroundColor: "#2e7d32",
            borderWidth: 0,
            borderRadius: 1.5,
            height: 36,
            width: 70,
          }}
          disabled={validateInputs()}
          onClick={onClick}
        >
          SAVE
        </Box>
      </div>
    </>
  );
}
