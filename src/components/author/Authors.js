import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import styled from "@emotion/styled";
import { Button, CircularProgress } from "@mui/material";
import BasicModal from "./../BasicModal";
import { fetchAllAuthors } from "../../api/graphql/query/Author";
import { Box } from "@mui/system";
const Authors = () => {
  const [rowData, setRowData] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState({ status: true, msg: "" });
  const [reload, setReload] = React.useState(true);

  const [open, setOpen] = React.useState({ target: "", status: false });

  const columns = [
    {
      field: "id",
      headerName: "ID",
      width: 180,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "firstName",
      headerName: "First name",
      width: 180,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "surName",
      headerName: "Last name",
      width: 180,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "dob",
      headerName: "Date of Birth",
      type: "date",
      width: 180,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "action",
      headerName: "Action Button",
      width: 300,
      headerAlign: "center",
      align: "center",
      sortable: false,
      renderCell: (params) => {
        const onClick = (e) => {
          e.stopPropagation(); // don't select this row after clicking
          setOpen({
            target: e.target.id,
            status: true,
            data: params.row,
          });
        };

        return (
          <>
            <Button
              variant="contained"
              color="success"
              id="edit-author"
              onClick={onClick}
            >
              Edit
            </Button>
            <Button
              sx={{ ml: 1, mr: 1 }}
              variant="contained"
              color="error"
              onClick={onClick}
              id="delete-author"
            >
              Delete
            </Button>
          </>
        );
      },
    },
  ];

  const Container = styled.div((props) => ({
    flexGrow: 1,
    marginTop: "5%",
    marginLeft: "10%",
    alignItems: "center",
    justifyContent: "center",
    width: "80%",
    height: "80%",
  }));

  React.useEffect(() => {
    fetchAllAuthors({ setIsLoading: setIsLoading, setData: setRowData });
  }, [reload]);

  return (
    <Container>
      {isLoading.status ? (
        <CircularProgress />
      ) : (
        <>
          <div style={{ height: 400, width: "100%" }}>
            <Box
              style={{
                display: "flex",
                justifyContent: "flex-end",
              }}
            >
              <Button
                sx={{ mb: 1, display: "flex", justifyContent: "flex-end" }}
                variant="contained"
                color="info"
                id="add-author"
                onClick={(e) => {
                  setOpen({
                    target: e.target.id,
                    status: true,
                  });
                }}
              >
                Add-Author
              </Button>
            </Box>
            <DataGrid
              style={{ justifyContent: "space-between", width: "100%" }}
              rows={rowData}
              columns={columns}
              pageSize={5}
              rowsPerPageOptions={[5]}
            />
          </div>
          {open.status && (
            <BasicModal
              data={open}
              setOpen={setOpen}
              setReload={setReload}
              reload={reload}
            />
          )}
        </>
      )}
    </Container>
  );
};
export default Authors;
