import * as React from "react";
import Box from "@mui/material/Box";
import {
  CircularProgress,
  Typography,
} from "@mui/material";
import AuthorFormView from "./AuthorFormView";
import { updateAuthor } from "../../api/graphql/mutation/Author";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function AuthorEditView({ row, ...props }) {
  const [data, setData] = React.useState({ ...row });
  const [isLoading, setIsLoading] = React.useState({
    status: false,
    msg: null,
  });

  return isLoading.status ? (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "-webkit-fill-available",
      }}
    >
      <CircularProgress />
    </div>
  ) : (
    <>
      <Box
        component="form"
        sx={[
          style,
          {
            "& .MuiTextField-root": { m: 1, width: "25ch" },
            borderRadius: 5,
          },
        ]}
        autoComplete="off"
      >
        {isLoading.msg != null ? (
          <Typography id="modal-modal-description">{isLoading.msg}</Typography>
        ) : (
          <>
            <AuthorFormView
              data={data}
              setData={setData}
              onClick={() => {
                setIsLoading({ status: true, msg: null });
                updateAuthor({ setIsLoading: setIsLoading, data: data });
              }}
            />
          </>
        )}
      </Box>
    </>
  );
}
