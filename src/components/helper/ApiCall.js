import configData from "./../../config.json";
import axios from "axios";

export const apiCall = async (params) => {
  const { method, data, headers, resFun, errFun } = params;
  var options = {
    method: method == null ? "POST" : method,
    headers: headers === undefined ? configData.headers : headers,
    data: data,
    url: configData.API_BASE_URL,
  };
  await axios(options)
    .then((response) => {
      resFun !== undefined && resFun(response);
    })
    .catch((error) => {
      errFun !== undefined && errFun(error);
    });
};
