import * as React from "react";
import Box from "@mui/material/Box";
import { CircularProgress, Typography } from "@mui/material";
import { fetchAllAuthors } from "../../api/graphql/query/Author";
import { addBook } from "../../api/graphql/mutation/Book";
import BookFormView from "./BookFormView";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function CreateView() {
  const [data, setData] = React.useState({
    title: null,
    publishYear: Date.now(),
    genre: null,
    authorIds: [],
  });

  const [isLoading, setIsLoading] = React.useState({ status: true, msg: null });
  const [authors, setAuthors] = React.useState([]);
  React.useEffect(() => {
    fetchAllAuthors({ setIsLoading: setIsLoading, setData: setAuthors });
  }, []);

  return isLoading.status ? (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "-webkit-fill-available",
      }}
    >
      {isLoading.msg != null ? (
        <Typography id="modal-modal-description">{isLoading.msg}</Typography>
      ) : (
        <CircularProgress />
      )}
    </div>
  ) : (
    <>
      <Box
        component="form"
        sx={[
          style,
          {
            "& .MuiTextField-root": { m: 1, width: "25ch" },
            borderRadius: 5,
          },
        ]}
        autoComplete="off"
      >
        {isLoading.msg != null ? (
          <Typography id="modal-modal-description">{isLoading.msg}</Typography>
        ) : (
          <BookFormView
            data={data}
            setData={setData}
            authors={authors}
            onClick={() => {
              setIsLoading({ status: true, msg: null });
              addBook({ setIsLoading: setIsLoading, data: data });
            }}
          />
        )}
      </Box>
    </>
  );
}
