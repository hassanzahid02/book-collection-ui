import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import styled from "@emotion/styled";
import { Box, Button, CircularProgress } from "@mui/material";
import BasicModal from "../BasicModal";
import { fetchBooks } from "../../api/graphql/query/Book";
const Books = () => {
  const [rowData, setRowData] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);
  const [open, setOpen] = React.useState({ target: "", status: false });
  const [reload, setReload] = React.useState(true);

  const columns = [
    { field: "id", headerName: "ID", width: 140, headerAlign: "center",  align: 'center', },
    {
      field: "title",
      headerName: "First name",
      width: 80,
      headerAlign: "center",
      align: 'center',
    },
    {
      field: "publishYear",
      headerName: "Last name",
      width: 140,
      headerAlign: "center",
      align: 'center',
    },
    {
      field: "genre",
      headerName: "Genre",
      type: "number",
      width: 230,
      headerAlign: "center",
      align: 'center',
    },
    {
      field: "author",
      headerName: "Author",
      width: 200,
      headerAlign: "center",
      align: 'center',
      renderCell: (params) => {
        const onClick = (e) => {
          e.stopPropagation(); // don't select this row after clicking
          setOpen({
            target: e.target.id,
            status: true,
            data: params.row,
          });
        };
        return params.row.authors.length > 0 ? (
          <Button onClick={onClick} id="authors">
            See authors detail
          </Button>
        ) : (
          "No author"
        );
      },
    },
    {
      field: "action",
      headerName: "Action Button",
      headerAlign: "center",
      width: 240,
      align: 'center',
      sortable: false,
      renderCell: (params) => {
        const onClick = (e) => {
          e.stopPropagation(); // don't select this row after clicking
          setOpen({
            target: e.target.id,
            status: true,
            data: params.row,
          });
        };
        return (
          <>
            <Button
              variant="contained"
              color="success"
              id="edit-book"
              onClick={onClick}
            >
              Edit
            </Button>
            <Button
              sx={{ ml: 1, mr: 1 }}
              variant="contained"
              color="error"
              onClick={onClick}
              id="delete-book"
            >
              Delete
            </Button>
          </>
        );
      },
    },
  ];

  const Container = styled.div((props) => ({
    flexGrow: 1,
    marginTop: "5%",
    marginLeft: "10%",
    alignItems: "center",
    justifyContent: "center",
    width: "80%",
    height: "80%",
  }));

  React.useEffect(() => {
    fetchBooks({ setIsLoading: setIsLoading, setData: setRowData });
  }, [reload]);

  return (
    <Container>
      {isLoading ? (
        <CircularProgress />
      ) : (
        <>
          <div style={{ height: 400, mt: 5, width: "100%" }}>
            <Box
              style={{
                display: "flex",
                justifyContent: "flex-end",
              }}
            >
              <Button
                sx={{ mb: 1 }}
                variant="contained"
                color="info"
                id="add-book"
                onClick={(e) => {
                  setOpen({
                    target: e.target.id,
                    status: true,
                  });
                }}
              >
                Add-Book
              </Button>
            </Box>
            <DataGrid
              rows={rowData}
              columns={columns}
              pageSize={5}
              rowsPerPageOptions={[5]}
            />
          </div>
          {open.status && (
            <BasicModal
              data={open}
              setOpen={setOpen}
              setReload={setReload}
              reload={reload}
            />
          )}
        </>
      )}
    </Container>
  );
};
export default Books;
