import * as React from "react";
import Box from "@mui/material/Box";
import {
  Checkbox,
  Chip,
  FormControl,
  InputLabel,
  ListItemText,
  MenuItem,
  OutlinedInput,
  Select,
  TextField,
} from "@mui/material";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import configData from "./../../config.json";

export default function BookFormView({ data, setData, onClick, authors }) {
  function validateInputs() {
    return (
      data.title == null && data.genre == null && data.authorIds.length <= 0
    );
  }
  return (
    <>
      <div>
        <TextField
          id="title"
          label="Title"
          value={data.title}
          required
          onChange={(e) => setData({ ...data, title: e.target.value })}
        />
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DatePicker
            label="Publish Year"
            value={data.publishYear}
            onChange={(newValue) => {
              setData({ ...data, publishYear: newValue });
            }}
            required
            renderInput={(params) => <TextField {...params} />}
          />
        </LocalizationProvider>
        <TextField
          id="outlined-select-currency"
          select
          label="Select Genre"
          value={data.genre}
          required
          onChange={(e) => {
            setData({ ...data, genre: e.target.value });
          }}
          helperText="Please select book genre"
        >
          {configData.genre.map((option) => (
            <MenuItem key={option["name"]} value={option["name"]}>
              {option["name"]}
            </MenuItem>
          ))}
        </TextField>
        <FormControl sx={{ m: 1, width: 225 }}>
          <InputLabel id="demo-multiple-checkbox-label">
            Select Author*
          </InputLabel>
          <Select
            labelId="demo-multiple-checkbox-label"
            id="demo-multiple-checkbox"
            multiple
            value={data.authorIds}
            onChange={(e) => {
              setData({ ...data, authorIds: e.target.value });
            }}
            input={<OutlinedInput label="Tag" />}
            renderValue={(selected) => (
              <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                {selected
                  .map((i) => authors.find((item) => item.id == i))
                  .map((value) => (
                    <Chip key={value.firstName} label={value.firstName} />
                  ))}
              </Box>
            )}
            MenuProps={{
              PaperProps: {
                style: {
                  maxHeight: 48 * 4.5 + 8,
                  width: 250,
                },
              },
            }}
          >
            {authors.map((option) => (
              <MenuItem key={option.id} value={option.id}>
                <Checkbox checked={data.authorIds.indexOf(option.id) > -1} />
                <ListItemText primary={option.firstName} />
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
      <div
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          margin: "auto",
          display: "flex",
        }}
      >
        <Box
          component="button"
          sx={{
            color: "white",
            backgroundColor: "#2e7d32",
            borderWidth: 0,
            borderRadius: 1.5,
            height: 36,
            width: 70,
          }}
          disabled={validateInputs()}
          onClick={onClick}
        >
          SAVE
        </Box>
      </div>
    </>
  );
}
