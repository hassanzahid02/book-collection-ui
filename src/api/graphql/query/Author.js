import * as React from "react";
import { apiCall } from "./../../../components/helper/ApiCall";
export const fetchAllAuthors = ({ setIsLoading, setData }) => {
  var data = JSON.stringify({
    query: `query {
    fetchAllAuthors{
      id
      firstName
      surName
      dob
    }
  }`,
    variables: {},
  });
  apiCall({
    data: data,
    resFun: (res) => {
      setData(res.data.data.fetchAllAuthors);
      setIsLoading({ status: false, msg: null });
    },
    errFun: (err) => {},
  });
};
