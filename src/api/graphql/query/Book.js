import * as React from "react";
import { apiCall } from "./../../../components/helper/ApiCall";
export const fetchBooks = ({ setIsLoading, setData }) => {
  var data = JSON.stringify({
    query: `query {
      fetchBooks {
        id
        title
        publishYear
        genre
        authors {
            id
            firstName
            surName
            dob
        }
      }
    }`,
    variables: {},
  });
  apiCall({
    data: data,
    resFun: (res) => {
      setData(res.data.data.fetchBooks);
      setIsLoading(false);
    },
    errFun: (err) => {},
  });
};

export const deleteBook = ({ id, setIsLoading }) => {
  var data = JSON.stringify({
    query: `mutation {
      deleteBook(input: { bookId: ${id}}) {
        book {
          id
        }
      }
    }`,
    variables: {},
  });
  apiCall({
    data: data,
    resFun: (res) => {
      if (res.data.errors?.length > 0)
        setIsLoading({ status: false, msg: res.data.errors[0].message });
      else {
        setIsLoading({ status: false, msg: "Book deleted successfully" });
      }
    },
  });
};
