import * as React from "react";
import { apiCall } from "./../../../components/helper/ApiCall";
export const updateAuthor = ({ setIsLoading, data }) => {
  let dob = new Date(data.dob).toISOString().slice(0, 10)
  var queryData = JSON.stringify({
    query: `mutation {
      updateAuthor(input: { authorId: "${data.id}", firstName: "${data.firstName}", dob: "${dob}", surName: "${data.surName}" }) {
        author {
          id
          firstName
          books {
              id
              title
          }
        }
      }
    }`,
    variables: {},
  });
  apiCall({
    data: queryData,
    resFun: (res) => {
      setIsLoading({
        status: false,
        msg: "Author updated successfully",
      });
    },
    errFun: (err) => {
      setIsLoading({
        msg: null,
        status: false,
      });
    },
  });
};

export const deleteAuthor = ({ setIsLoading, id }) => {
  var data = JSON.stringify({
    query: `mutation {
    deleteAuthor(input: { authorId: ${id}}) {
      author {
        id
      }
    }
  }`,
    variables: {},
  });
  apiCall({
    data: data,
    resFun: (res) => {
      if (res.data.errors?.length > 0)
        setIsLoading({ status: false, msg: res.data.errors[0].message });
      else {
        setIsLoading({ status: false, msg: "Author deleted successfully" });
      }
    },
  });
};

export const addAuthor = ({ setIsLoading, data }) => {
  var queryData = JSON.stringify({
    query: `mutation {
        addAuthor(input: { params: { firstName: "${data.firstName}", dob: "2020-02-08", surName: "${data.surName}" }}) {
          author {
            id
            firstName
            books {
                id
                title
            }
          }
        }
      }`,
    variables: {},
  });
  apiCall({
    data: queryData,
    resFun: (res) => {
      setIsLoading({
        status: false,
        msg: "Author added successfully",
      });
    },
    errFun: (err) => {
      setIsLoading({
        msg: null,
        status: false,
      });
    },
  });
};
