import * as React from "react";
import { apiCall } from "./../../../components/helper/ApiCall";
export const updateBook = ({ setIsLoading, data }) => {
  let publishYear = new Date(data.publishYear).toISOString().slice(0, 10)
  var queryData = JSON.stringify({
    query: `mutation {
        updateBook(input: {
        bookId: "${data.id}",
        title: "${data.title}",
        publishYear: "${publishYear}",
        genre: "${data.genre}",
        authorIds: ${JSON.stringify(data.authorIds)},
      }) {
        book {
          id
          title
          genre
          authors {
              id
              firstName
          }
        }
      }
    }`,
    variables: {},
  });
  apiCall({
    data: queryData,
    resFun: (res) => {
      setIsLoading({
        status: false,
        msg: "Book updated successfully",
      });
    },
    errFun: (err) => {
      setIsLoading({
        msg: null,
        status: false,
      });
    },
  });
};

export const addBook = ({ setIsLoading, data }) => {
  var queryData = JSON.stringify({
    query: `mutation {
      addBook(input: { params: {
        title: "${data.title}",
        publishYear: "2020-02-08",
        genre: "${data.genre}",
        authorIds: ${JSON.stringify(data.authorIds)},
      }}) {
        book {
          id
          title
          genre
          authors {
              id
              firstName
          }
        }
      }
    }`,
    variables: {},
  });
  apiCall({
    data: queryData,
    resFun: (res) => {
      setIsLoading({
        status: false,
        msg: "Book added successfully",
      });
    },
    errFun: (err) => {
      setIsLoading({
        msg: null,
        status: false,
      });
    },
  });
};
