import React from 'react';
import { BrowserRouter, Switch } from 'react-router-dom'
import AppRoute from "./components/AppRoute";
import "./App.css";

function App() {
  return (
    <BrowserRouter>
			<Switch>
				<AppRoute path='/' />
			</Switch>
		</BrowserRouter>
  )
}

export default App;
