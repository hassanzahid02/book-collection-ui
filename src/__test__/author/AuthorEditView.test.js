import React from 'react';
import { create, act } from 'react-test-renderer';

import AuthorEditView from './../../components/author/AuthorEditView';

it('renders author edit view correctly', () => {
  act(() => {
    let row = {
      id: Math.floor(Math.random() * 20),
      firstName: 'test',
      surName: 'author',
      dob: new Date()
    }
    const tree = create(<AuthorEditView />).toJSON();
    expect(tree).toMatchSnapshot();
  })
});