import React from 'react';
import { create, act } from 'react-test-renderer';

import AuthorCreateView from './../../components/author/AuthorCreateView';

it('renders author create view correctly', () => {
  act(() => {
    const tree = create(<AuthorCreateView />).toJSON();
    expect(tree).toMatchSnapshot();
  })
});