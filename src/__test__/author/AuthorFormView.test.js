import React from 'react';
import { create, act } from 'react-test-renderer';

import AuthorFormView from './../../components/author/AuthorFormView';

it('renders authors correctly', () => {
  let data = {
    firstName: null,
    dob: Date.now(),
    surName: null,
  }
  const setData = jest.fn();
  const onClick = jest.fn();
  act(() => {
    const tree = create(<AuthorFormView data={data} setData={setData} onClick={onClick}/>).toJSON();
    expect(tree).toMatchSnapshot();
  })
});