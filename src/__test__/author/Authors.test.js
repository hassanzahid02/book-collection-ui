import React from 'react';
import { create, act } from 'react-test-renderer';

import Authors from './../../components/author/Authors';

it('renders authors correctly', () => {
  act(() => {
    const tree = create(<Authors />).toJSON();
    expect(tree).toMatchSnapshot();
  })
});