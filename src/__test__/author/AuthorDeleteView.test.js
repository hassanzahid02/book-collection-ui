import React from 'react';
import { create, act } from 'react-test-renderer';

import AuthorDeleteView from './../../components/author/AuthorDeleteView';

it('renders author delete view correctly', () => {
  let row = {
    id: Math.floor(Math.random() * 20)
  }
  act(() => {
    const tree = create(<AuthorDeleteView row={row}/>).toJSON();
    expect(tree).toMatchSnapshot();
  })
});