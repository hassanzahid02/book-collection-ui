/**
 * @jest-environment jsdom
 */
import React, {useState} from 'react';
import { create, act } from 'react-test-renderer';

import BookFormView from './../../components/book/BookFormView';

it('renders book form correctly', () => {
  let authors = []
  let data = {
    title: null,
    publishYear: Date.now(),
    genre: null,
    authorIds: [],
  }
  const setData = jest.fn();
  const onClick = jest.fn();
  act(() => {
    const tree = create(<BookFormView data={data} setData={setData} onClick={onClick} authors={authors}/>).toJSON();
    expect(tree).toMatchSnapshot();
  })
});