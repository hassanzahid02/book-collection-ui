import React from 'react';
import { create, act } from 'react-test-renderer';

import CreateView from './../../components/book/CreateView';

it('renders create book view correctly', () => {
  act(() => {
    const tree = create(<CreateView />).toJSON();
    expect(tree).toMatchSnapshot();
  })
});