import React from 'react';
import { create, act } from 'react-test-renderer';

import DeleteView from './../../components/book/DeleteView';

it('renders delete book view correctly', () => {
  let row = {
    id: Math.floor(Math.random() * 20)
  }
  act(() => {
    const tree = create(<DeleteView row={row}/>).toJSON();
    expect(tree).toMatchSnapshot();
  })
});