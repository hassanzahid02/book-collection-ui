import React from 'react';
import { create, act } from 'react-test-renderer';

import Books from './../../components/book/Books';

it('renders books correctly', () => {
  act(() => {
    const tree = create(<Books />).toJSON();
    expect(tree).toMatchSnapshot();
  })
});