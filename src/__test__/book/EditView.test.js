import React from 'react';
import { create, act } from 'react-test-renderer';

import EditView from './../../components/book/EditView';

it('renders edit book view correctly', () => {
  let row = {
    authors: [{
      id: Math.floor(Math.random() * 20),
      firstName: 'test',
      surName: 'author',
      dob: new Date()
    }]
  }
  act(() => {
    const tree = create(<EditView row={row}/>).toJSON();
    expect(tree).toMatchSnapshot();
  })
});