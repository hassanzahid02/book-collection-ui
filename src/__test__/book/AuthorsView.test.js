/**
 * @jest-environment jsdom
 */
import React from 'react';
import { create, act } from 'react-test-renderer';

import AuthorsView from './../../components/book/AuthorsView';

it('renders authors correctly', () => {
  let row = {
    authors: [{
      id: Math.floor(Math.random() * 20),
      firstName: 'test',
      surName: 'author',
      dob: new Date()
    }]
  }
  act(() => {
    const tree = create(<AuthorsView row={row} />).toJSON();
    expect(tree).toMatchSnapshot();
  })
});